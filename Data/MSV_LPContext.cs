﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MSV_LP.Models;

namespace MSV_LP.Data
{
    public class MSV_LPContext : DbContext
    {
        public MSV_LPContext (DbContextOptions<MSV_LPContext> options)
            : base(options)
        {
        }

        public DbSet<MSV_LP.Models.FaultLog> FaultLog { get; set; }

        public DbSet<MSV_LP.Models.Fault> Fault { get; set; }
    }
}
