﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MSV_LP.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Fault",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogID = table.Column<int>(nullable: false),
                    zkratka = table.Column<string>(nullable: true),
                    datum = table.Column<DateTime>(nullable: false),
                    cas = table.Column<string>(nullable: true),
                    trvani = table.Column<string>(nullable: true),
                    zdrBit = table.Column<string>(nullable: true),
                    rychlost = table.Column<int>(nullable: false),
                    PT = table.Column<string>(nullable: true),
                    kontext = table.Column<string>(nullable: true),
                    CisloPor = table.Column<string>(nullable: true),
                    byte0 = table.Column<int>(nullable: false),
                    byte1 = table.Column<int>(nullable: false),
                    byte2 = table.Column<int>(nullable: false),
                    byte3 = table.Column<int>(nullable: false),
                    drahaH = table.Column<int>(nullable: false),
                    drahaL = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fault", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "FaultLog",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Locomotive = table.Column<string>(nullable: true),
                    DownloadDate = table.Column<DateTime>(nullable: false),
                    DownloadTime = table.Column<DateTime>(nullable: false),
                    Odometr = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaultLog", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Fault");

            migrationBuilder.DropTable(
                name: "FaultLog");
        }
    }
}
