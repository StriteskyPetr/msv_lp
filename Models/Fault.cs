﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MSV_LP.Models
{
    public class Fault
    {
        [Key]
        public int ID { get; set; }
        public int LogID { get; set; } //foreign key
        public string zkratka { get; set; }

        [DataType(DataType.Date)]
        public DateTime datum { get; set; }

        public string cas { get; set; }
        public string trvani { get; set; }
        public string zdrBit { get; set; }
        public int rychlost { get; set; }
        public string PT { get; set; }
        public string kontext { get; set; }
        public string CisloPor { get; set;}
        public ushort byte0 { get; set; }
        public ushort byte1 { get; set; }
        public ushort byte2 { get; set; }
        public ushort byte3 { get; set; }
        public int drahaH { get; set; }
        public int drahaL { get; set; }
    }
}
