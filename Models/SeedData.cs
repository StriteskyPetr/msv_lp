﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MSV_LP.Data;
using System;
using System.Linq;

namespace MSV_LP.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MSV_LPContext(serviceProvider.GetRequiredService<DbContextOptions<MSV_LPContext>>()))
            {
                // Look for any movies.
                if (context.FaultLog.Any())
                {
                    return;   // DB has been seeded
                }
            }
        }
    }
}
