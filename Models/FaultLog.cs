﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MSV_LP.Models
{
    public class FaultLog
    {
        [Key]
        public int ID { get; set; }
        public string Locomotive { get; set; }

        [DataType(DataType.Date)]
        public DateTime DownloadDate { get; set; }
        
        [DataType(DataType.Time)]
        public DateTime DownloadTime { get; set; }
        
        public float Odometr { get; set; }
    }
}
