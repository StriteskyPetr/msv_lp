﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MSV_LP.Data;
using MSV_LP.Models;

namespace MSV_LP.Pages.Faults
{
    public class EditModel : PageModel
    {
        private readonly MSV_LP.Data.MSV_LPContext _context;

        public EditModel(MSV_LP.Data.MSV_LPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Fault Fault { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Fault = await _context.Fault.FirstOrDefaultAsync(m => m.ID == id);

            if (Fault == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Fault).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FaultExists(Fault.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool FaultExists(int id)
        {
            return _context.Fault.Any(e => e.ID == id);
        }
    }
}
