﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MSV_LP.Data;
using MSV_LP.Models;

namespace MSV_LP.Pages.Faults
{
    public class IndexModel : PageModel
    {
        private readonly MSV_LP.Data.MSV_LPContext _context;

        public IndexModel(MSV_LP.Data.MSV_LPContext context)
        {
            _context = context;
        }

        public IList<Fault> Fault { get; set; }

        public IList<FaultLog> FaultLog { get; set; }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }


        [BindProperty(SupportsGet = true)]
        public string LocomotiveNo { get; set; }


        [BindProperty(SupportsGet = true)]
        public string FaultSearchString { get; set; }


        [BindProperty(SupportsGet = true)]
        public string DateNo { get; set; }

        public SelectList locoList { get; set; }
        public SelectList faultList { get; set; }
        public SelectList dateList { get; set; }
       

        public async Task OnGetAsync()
        {
            
            IQueryable<string> LocoQuery = from m in _context.FaultLog
                                            orderby m.Locomotive
                                            select m.Locomotive;

            IQueryable<string> FaultQuery = from o in _context.Fault
                                             orderby o.zkratka
                                             select o.zkratka;

            IQueryable<string> DateQuery = from n in _context.Fault
                                           orderby n.datum.ToString().Substring(0, 10)
                                           select n.datum.ToString().Substring(0,10);

            var FaultLogs = from m in _context.FaultLog
                              select m;

            var Faults = from n in _context.Fault
                        select n;
            
            if (!string.IsNullOrEmpty(SearchString))
            {
                FaultLogs = FaultLogs.Where(s => s.Locomotive.Contains(SearchString));
            }
            if (!string.IsNullOrEmpty(FaultSearchString))
            {
                Faults = Faults.Where(y => y.zkratka == FaultSearchString);
            }
            if (!string.IsNullOrEmpty(DateNo))
                {
                Faults = Faults.Where(z => z.datum.ToString().Substring(0,10) == DateNo);
            }
            if (!string.IsNullOrEmpty(LocomotiveNo))
            {
                FaultLogs = FaultLogs.Where(x => x.Locomotive == LocomotiveNo);
            }
            faultList = new SelectList(await FaultQuery.Distinct().ToListAsync());
            locoList = new SelectList(await LocoQuery.Distinct().ToListAsync());
            dateList = new SelectList(await DateQuery.Distinct().ToListAsync());

            FaultLog = await FaultLogs.ToListAsync();
            Fault = await Faults.ToListAsync();
            //Fault = await _context.Fault.ToListAsync();
        }
    }
}
