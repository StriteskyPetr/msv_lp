﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using MSV_LP.Data;
using MSV_LP.Models;
using MSV_LP.Utilities;
using System.IO;
using System.Globalization;

namespace MSV_LP.Pages.FaultLogs
{
    public class UploadModel : PageModel
    {
        private readonly MSV_LP.Data.MSV_LPContext _context;
        //private readonly long _fileSizeLimit;
        private readonly string[] _permittedExtensions = { ".his" };

        public UploadModel(MSV_LP.Data.MSV_LPContext context, IConfiguration config)
        {
            _context = context;
            //_fileSizeLimit = config.GetValue<long>("FileSizeLimit");
        }

        [BindProperty]
        public BufferedSingleFileUploadDb FileUpload { get; set; }

        public string Result { get; private set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostUploadAsync()
        {
            // Perform an initial check to catch FileUpload class
            // attribute violations.
            if (!ModelState.IsValid)
            {
                Result = "Please correct the form.";

                return Page();
            }

            var formFileContent = await FileHelpers.ProcessFormFile<BufferedSingleFileUploadDb>(FileUpload.FormFile, ModelState, _permittedExtensions);

            // Perform a second check to catch ProcessFormFile method
            // violations. If any validation check fails, return to the
            // page.
            if (!ModelState.IsValid)
            {
                Result = "Please correct the form.";

                return Page();
            }

            // **WARNING!**
            // In the following example, the file is saved without
            // scanning the file's contents. In most production
            // scenarios, an anti-virus/anti-malware scanner API
            // is used on the file before making the file available
            // for download or for use by other systems. 
            // For more information, see the topic that accompanies 
            // this sample.

            //zpracovani souboru
            string result = System.Text.Encoding.UTF8.GetString(formFileContent);
            StringReader reader = new StringReader(result);
            string header = reader.ReadLine();
            string Locomotive = header.Substring(4, 7);
            string D_D = header.Substring(15, 8);
            string D_T = header.Substring(25, 8);
            string dateYear = D_D.Substring(0, 3);
            DateTime DownloadDate = DateTime.ParseExact(D_D, "yy-MM-dd", null);//log download date
            DateTime DownloadTime = DateTime.ParseExact(D_T, "HH:mm:ss", null);//
            float Odo = float.Parse(header.Substring(46, 9), CultureInfo.InvariantCulture.NumberFormat);


            /* creation of the FaultLog */
            var file = new FaultLog
            {
                Locomotive = Locomotive,
                DownloadDate = DownloadDate,
                DownloadTime = DownloadTime,
                Odometr = Odo
            };

            int firstFaultLine = 5; // number of lines to skip
            /* skip empty or unnecessary lines */
            for (int i = 1; i < firstFaultLine; i++)
            {
                reader.ReadLine();
            }

            /* creation of the Fault entry */
            while (reader.ReadLine() != null) //read till the end of file
            {
                string Entry = reader.ReadLine();
                string date = Entry.Substring(6, 5);
                string dateComplete = string.Concat(dateYear, date);
                var faultEntry = new Fault
                {
                    zkratka = Entry.Substring(0, 4),
                    datum = DateTime.ParseExact(dateComplete, "yy-MM-dd", null),
                    cas = Entry.Substring(13, 8),
                    trvani = Entry.Substring(24, 5),
                    zdrBit = Entry.Substring(30, 4),
                    rychlost = int.Parse(Entry.Substring(35, 3)),
                    PT = Entry.Substring(40, 4),
                    kontext = Entry.Substring(45, 4),
                    CisloPor = Entry.Substring(50, 4),
                    byte0 = ushort.Parse(Entry.Substring(55, 3)),
                    byte1 = ushort.Parse(Entry.Substring(59, 3)),
                    byte2 = ushort.Parse(Entry.Substring(63, 3)),
                    byte3 = ushort.Parse(Entry.Substring(67, 3)),
                    drahaH = int.Parse(Entry.Substring(71, 7))//,
                    //drahaL = int.Parse(Entry.Substring(79, 7))
                };
                _context.Fault.Add(faultEntry);
                await _context.SaveChangesAsync();
            }

            _context.FaultLog.Add(file);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }

    public class BufferedSingleFileUploadDb
    {
        [Required]
        [Display(Name = "File")]
        public IFormFile FormFile { get; set; }

        [Display(Name = "Note")]
        [StringLength(50, MinimumLength = 0)]
        public string Note { get; set; }
    }

}



