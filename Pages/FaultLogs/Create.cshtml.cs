﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MSV_LP.Data;
using MSV_LP.Models;

namespace MSV_LP.Pages.FaultLogs
{
    public class CreateModel : PageModel
    {
        private readonly MSV_LP.Data.MSV_LPContext _context;

        public CreateModel(MSV_LP.Data.MSV_LPContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public FaultLog FaultLog { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.FaultLog.Add(FaultLog);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
