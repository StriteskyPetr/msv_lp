﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MSV_LP.Data;
using MSV_LP.Models;

namespace MSV_LP.Pages.FaultLogs
{
    public class DeleteModel : PageModel
    {
        private readonly MSV_LP.Data.MSV_LPContext _context;

        public DeleteModel(MSV_LP.Data.MSV_LPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public FaultLog FaultLog { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FaultLog = await _context.FaultLog.FirstOrDefaultAsync(m => m.ID == id);

            if (FaultLog == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FaultLog = await _context.FaultLog.FindAsync(id);

            if (FaultLog != null)
            {
                _context.FaultLog.Remove(FaultLog);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
