﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MSV_LP.Data;
using MSV_LP.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MSV_LP.Pages.FaultLogs
{
    public class IndexModel : PageModel
    {
        private readonly MSV_LP.Data.MSV_LPContext _context;

        public IndexModel(MSV_LP.Data.MSV_LPContext context)
        {
            _context = context;
        }

        public IList<FaultLog> FaultLog { get;set; }
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        [BindProperty(SupportsGet = true)]
        public string LocomNo { get; set; }
        [BindProperty(SupportsGet = true)]
        public string dateNo { get; set; }

        public SelectList locoList { get; set; }
        public SelectList dateList { get; set; }

        public async Task OnGetAsync()
        {
            IQueryable<string> locoQuery = from m in _context.FaultLog
                                            orderby m.Locomotive
                                            select m.Locomotive;

            IQueryable<string> dateQuery = from n in _context.FaultLog
                                           orderby n.DownloadDate.ToString().Substring(0, 10)
                                           select n.DownloadDate.ToString().Substring(0,10);

            var Locos = from m in _context.FaultLog
                         select m;

            if (!string.IsNullOrEmpty(SearchString))
            {
                Locos = Locos.Where(s => s.Locomotive.Contains(SearchString));
            }

            if (!string.IsNullOrEmpty(dateNo))
            {
                Locos = Locos.Where(y => y.DownloadDate.ToString().Substring(0, 10) == dateNo);
            }

            if (!string.IsNullOrEmpty(LocomNo))
            {
                Locos = Locos.Where(x => x.Locomotive == LocomNo);
            }
            

            dateList = new SelectList(await dateQuery.Distinct().ToListAsync());
            locoList = new SelectList(await locoQuery.Distinct().ToListAsync());
            FaultLog = await Locos.ToListAsync();
        }
    }
}
